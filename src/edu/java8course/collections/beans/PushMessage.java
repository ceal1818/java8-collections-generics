package edu.java8course.collections.beans;

public class PushMessage extends Message {

	private String device;

	public PushMessage(int id, String type, String content, String device) {
		super(id, type, content);
		this.device = device;
	}

	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}
	
	@Override
	public String toString() {
		String result = super.toString()+" [PushMessage]: "+this.getDevice();
		return result;
	}
	
}
