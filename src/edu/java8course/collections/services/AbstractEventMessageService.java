package edu.java8course.collections.services;

import edu.java8course.collections.beans.EventMessage;

public abstract class AbstractEventMessageService implements MessageService<EventMessage> {

	public AbstractEventMessageService() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void notify(EventMessage message) {
		// TODO Auto-generated method stub
		System.out.println(message.toString());
	}

	@Override
	public void notifyAll(EventMessage message) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void listen(EventMessage message) {
		// TODO Auto-generated method stub
	}

}
