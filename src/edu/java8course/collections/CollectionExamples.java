package edu.java8course.collections;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CollectionExamples {

	public static void main(String[] args) {
		List<Integer> arr1 = new ArrayList<>();

		for (int i = 10; i > 0; i--) {
			arr1.add(i);
		}
		
		for (int i = 0; i < arr1.size(); i++) {
			System.out.println("Element ["+i+"] : "+arr1.get(i));
		}
			
		for (Iterator<Integer> iter = arr1.iterator(); iter.hasNext();) {
			System.out.println("Element: "+iter.next());
		}
		
		arr1.forEach(v -> System.out.println("Element (Lambda): "+v));
		
		//System.out.println("Element [5]: "+arr1.get(5));
		
		arr1.remove(2);
		arr1.remove(new Integer(2));
		
		for (Iterator<Integer> iter = arr1.iterator(); iter.hasNext();) {
			System.out.println("Element: "+iter.next());
		}
		
		//System.out.println("Element [2]: "+arr1.get(2));
		
		Map<String, Integer> dictionary = new HashMap<>();
		
		dictionary.put("1", 1);
		dictionary.put("2", 2);
		dictionary.put("3", 3);
		
		System.out.println("Element of key '2': "+dictionary.get("2"));
		
		Set<String> keys = dictionary.keySet();
		
		for (Iterator<String> iter2 = keys.iterator(); iter2.hasNext();) {
			String key = iter2.next();
			System.out.println("Element ["+key+"]: "+dictionary.get(key));
		}

		LinkedList<Integer> linkedList = new LinkedList<>();
		
		for (int i = 10; i > 0; i--) {
			linkedList.add(i);
		}
		
		linkedList.forEach(v -> System.out.println("Element (Lambda): "+v));
		
		System.out.println(linkedList.getFirst());
		System.out.println(linkedList.getLast());
		
	}

}
